package ru.custis;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

public class RandomAccessFileWithEncoding extends RandomAccessFile {

    private final String encoding;

    public RandomAccessFileWithEncoding(String name, String mode, String encoding) throws FileNotFoundException {
        super(name, mode);
        this.encoding = encoding;
    }

    public String readLineWithEncoding() throws IOException {
        List<Byte> line = new ArrayList<>();
        for (int c = read(); c != 10;) {
            line.add((byte) c);
        }
        byte[] chars = new byte[line.size()];
        for (int i = 0; i<chars.length; ++i)
            chars[i] = line.get(i);
        return new String(chars, encoding);
    }
}
