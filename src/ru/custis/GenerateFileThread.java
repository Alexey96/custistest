package ru.custis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class GenerateFileThread extends Thread {
    private static final String FILE_PATH = System.getenv().get("TEMP")+File.separator+"custisexample.txt";
    private static Scanner scanner = new Scanner(System.in);
    private static final String ENCODING = "Cp-1251";
    final String tempName;
    Boolean fileGeneratedSuccessfully = false;
    private static ReentrantLock consoleLock = new ReentrantLock();
    private static Condition consoleCondition = consoleLock.newCondition();
    private static final Logger log = LoggerFactory.getLogger(GenerateFileThread.class);

    GenerateFileThread(String tempName) {
        this.tempName = tempName;
    }

    public static void main(String[] args) {
//        renameWithReplace(file, "C:\\Users\\ASUS\\Downloads\\renamed.txt");
//        System.getenv().entrySet().stream().forEach(entry -> System.out.println(entry.getKey()+"="+entry.getValue()));
log.info("test");
        JFrame frame = new JFrame("Messenger");
        long millis = System.currentTimeMillis();
        //String fName = null;
        GenerateFileThread generateFileThread;
        RenameFileThread renameFileThread = new RenameFileThread();

        if (args.length > 0 && !args[0].isEmpty())
            generateFileThread = new GenerateFileThread(renameFileThread.fName = args[0]);
        else generateFileThread = new GenerateFileThread(FILE_PATH);
        renameFileThread.setRenameFileThread(generateFileThread);

        generateFileThread.start();

        if (renameFileThread.fName == null) {
            if (askYNResult("Требуется полное имя! Использовать имя файла '" + FILE_PATH + "'?")) {
                renameFileThread.fName = FILE_PATH;
            } else {
                System.out.println("Введите полное имя файла");
                renameFileThread.fName = scanner.nextLine();
                renameFileThread.start();
                renameFileThread.successfullyRenamed = false;
            }
        }

        if (askYNResult("Отсортировать?")) {
            try {
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setSize(400, 300);
                frame.setLayout(new BorderLayout());
                final JProgressBar bar = new JProgressBar(0,100);
                bar.setValue(0);
                frame.add(bar);
                bar.setStringPainted(false);
                bar.setBounds(20,115,350,10);
                JButton buttonInterrupt = new JButton("Interrupt");
                frame.add(buttonInterrupt);
                buttonInterrupt.setBounds(100, 160, 160, 80);

                FileSorter fileSorter = new FileSorter(ENCODING, v -> bar.setValue(v));
                buttonInterrupt.addActionListener(e -> fileSorter.interrupt());
                    //-> Выше мы ждём завершения работы потока GenerateFileThread. Это ожидание можно сделать лучше посредством механизма wait/notifyAll
                    // либо Condition.await/signalAll, Condition получаем из ReentrantLock. Примеры см в книге
                consoleLock.lock();
                try {
                    while (!generateFileThread.fileGeneratedSuccessfully) {
                        consoleCondition.await();
                    }
                    while (!renameFileThread.successfullyRenamed){
                        consoleCondition.await();
                    }
                }
                finally {
                    consoleLock.unlock();
                }
                frame.setVisible(true);
                bar.setVisible(true);
                buttonInterrupt.setVisible(true);

                fileSorter.sort(renameFileThread.fName);
            } catch (FileNotFoundException e){
                System.err.println("Не удалось открыть файл "+ renameFileThread.fName);
                e.printStackTrace();
                System.exit(1);
            } catch (IOException e1){
                throw new RuntimeException("Ошибка при сортировке файла "+renameFileThread.fName, e1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Executed during " + (System.currentTimeMillis()-millis) + " milliseconds");
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
    }

    @Override
    public void run() {
        consoleLock.lock();
        log.info("Начало генерации");
        try {
            new RandomContentFile(50, 1000).generateFile(tempName);
            fileGeneratedSuccessfully = true;
            consoleCondition.signalAll();
//            System.out.print("Файл создан");
        } catch (FileNotFoundException e){
            System.err.println("Не удалось создать файл "+ tempName);
            e.printStackTrace();
            System.exit(1);
        } catch (Exception e0){
            throw new RuntimeException("Ошибка при генерации файла "+tempName, e0);
        } finally {
            consoleLock.unlock();
            log.info("Конец генерации");
        }
    }

    private static boolean renameWithReplace(File source, String newName){
        File checker = new File(newName);
        if (checker.exists() && !checker.delete()) {
             return false;
        }
        source.renameTo(checker);
        return true;
    }

    private static boolean askYNResult(String prePrompt){
        System.out.println(prePrompt + " y-да, иначе нет");
        return "y".equals(scanner.nextLine());
    }

    static class RenameFileThread extends Thread {
        private GenerateFileThread thread;
        String fName;
        Boolean successfullyRenamed = true;

        public void setRenameFileThread(GenerateFileThread thread) {
            this.thread = thread;
        }

        @Override
        public void run(){
            consoleLock.lock();
            log.info("Начало переименования");
            try {
                while (!thread.fileGeneratedSuccessfully) {
                    consoleCondition.await();
                }
                renameWithReplace(new File(thread.tempName), fName);
                successfullyRenamed = true;
                consoleCondition.signalAll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                consoleLock.unlock();
                log.info("Конец переименования");
            }
        }
    }
}
