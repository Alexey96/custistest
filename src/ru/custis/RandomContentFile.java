package ru.custis;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RandomContentFile {
    private final int maxLineLength;
    private final int lineNumbers;
    private final List<Integer> availableSymbols = new ArrayList<>();

    public RandomContentFile(int maxLineLength, int lineNumbers) {
        this.maxLineLength = maxLineLength;
        this.lineNumbers = lineNumbers;
        fillAvailableSymbols();
    }

    public void generateFile(String fileName) throws Exception {
        FileOutputStream fo = new FileOutputStream(fileName);

        for (int row = 0; row < lineNumbers; ++row) {
            int posInLine = 0;
            while (!isEndOfLine(posInLine++))
                fo.write(getRandomSymbol());
            fo.write(10); //Символ конца строки
            Thread.sleep(20L);
        }
        fo.close();
    }

    private void fillAvailableSymbols() {
        for (int c = 97; c < 123; ++c)
            availableSymbols.add(c);
    }

    private boolean isEndOfLine(int posInLine) {
        if (posInLine < 1)
            return false;
        return posInLine == maxLineLength || Math.random() < 1.0/maxLineLength;
    }

    private int getRandomSymbol() {
        return availableSymbols.get((int) (Math.random() * availableSymbols.size()));
    }
}
