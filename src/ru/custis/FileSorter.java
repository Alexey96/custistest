package ru.custis;

import java.io.IOException;
import java.util.function.Consumer;

public class FileSorter {

    private final String encoding;
    private final Consumer<Integer> progressHalter;
    private boolean isInterrupted;

    public FileSorter(String encoding, Consumer<Integer> progressHalter){
        this.encoding = encoding;
        this.progressHalter = progressHalter;
    }

    public void interrupt(){
        isInterrupted = true;
    }

    public void sort(String filepath) throws IOException {

        progressHalter.accept(0);
        RandomAccessFileWithEncoding file = null;
        try {
            file = new RandomAccessFileWithEncoding(filepath, "rw", encoding);
            boolean done;
            int startSwapsAmount = -1;
            do {
                done = true;
                file.seek(0);
                long pos1 = 0;
                String str1 = file.readLine();
                int curSwapsAmount = 0;
                while (file.getFilePointer() < file.length() && !isInterrupted) {
                    long pos2 = file.getFilePointer();
                    String str2 = file.readLine();
                    if (str1.compareTo(str2) > 0) {
                        done = false;
                        file.seek(pos1);
                        file.write((str2 + "\n" + str1).getBytes());
                        pos1 = pos1 + str2.length() + 1;
                        file.seek(pos1 + str1.length() + 1);
                        ++curSwapsAmount;
                    } else {
                        str1 = str2;
                        pos1 = pos2;
                    }
                }
                if (startSwapsAmount < 0)
                    startSwapsAmount = curSwapsAmount;
                else
                    progressHalter.accept(100 - curSwapsAmount * 100/startSwapsAmount);

            } while (!done && !isInterrupted);
        } finally {
            if (file != null)
                file.close();
        }

       // System.out.println("Work done");
    }
}
